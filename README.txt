Two WARs to be deployed on two separate Tomcat 7 servers.

Server One generates a random long and a random String every X second and sends them to Server Two.

Server Two stores these variables and sends them to the client every Y second.

X and Y can be altered at runtime by the client, which is an HTML website that can be launched from browsing to either server.

!IMPORTANT!
The URI in the HTML script and the Java ServerEndpoint must set before deployment. On the client side this variable can be found in the beginning of src/main/webapp/randomsocket.js on each server, and for Server Two the URI that needs to be set is located in src/main/resources/server_config.properties.