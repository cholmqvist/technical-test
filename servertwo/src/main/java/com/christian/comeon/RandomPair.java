package com.christian.comeon;

/**
 * A pair of Strings, one representing a random long
 * and one representing a random String
 * Created by christian on 12/06/15.
 */
public class RandomPair {
    private String mLong;
    private String mString;

    public RandomPair(String randLong, String randString) {
        mLong = randLong;
        mString = randString;
    }

    public void setLong(String randLong) {
        mLong = randLong;
    }

    public void setString(String randString) {
        mString = randString;
    }

    public String getLong() {
        return mLong;
    }

    public String getString() {
        return mString;
    }
}
