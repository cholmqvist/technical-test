package com.christian.comeon;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * A server endpoint class that accepts a connection from a client and
 * runs a ConnectorThread that handles that connection. It also opens a
 * websocket that acts as a client to another server that provides
 * continuously updated variables that the ConnectorThread uses in its
 * handling of this endpoint's connection.
 * Created by christian on 09/06/15.
 */
@ServerEndpoint("/randomServer")
public class ServerTwo {
    private Thread mThread;
    private ConnectorThread mConnector;
    private ServerWebsocket mWebSocket;
    private Session mSession;
    private RandomPair mPair;

    /**
     * When a connection is opened this method is called. It, in turn, opens a websocket
     * to another server that will provide it with data and runs a thread that relays
     * that information to this server's client.
     * @param session A session that represents the connection between this server and it client.
     */
    @OnOpen
    public void onOpen(Session session) {
        System.out.println(session.getId() + " has opened a connection");
        String uri = getServerURI();
        mSession = session;
        try {
            mWebSocket = new ServerWebsocket(new URI(uri));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        // Adds a message handler for the websocket that receives data from another server
        mWebSocket.addMessageHandler(new ServerWebsocket.MessageHandler() {
            @Override
            public void handleMessage(String message) throws IOException {
                Gson gson = new GsonBuilder().create();
                SocketMessage update = gson.fromJson(message, SocketMessage.class);
                switch (update.getKey()) {
                    case "random":
                        String[] randoms = update.getValue().split(" ");
                        mPair.setLong(randoms[0]);
                        mPair.setString(randoms[1]);
                        break;
                    case "request":
                        System.out.println(update.getValue());
                        break;
                    case "x":
                        System.out.println("Update of X has been handled by ServerOne");
                        break;
                    default:
                        System.out.println("Error: wrong message format from ServerOne");
                        System.out.println(message);
                }
            }
        });

        try {
            // Constructs a JSON message, which is the accepted format, and sends it to the client
            JsonObject json = new JsonObject();
            json.addProperty("mKey", "request");
            json.addProperty("mValue", "Connection established for random");
            session.getBasicRemote().sendText(json.toString());

            mPair = new RandomPair("", "");
            mConnector = new ConnectorThread(mSession, mPair, 2);
            mThread = new Thread(mConnector);
            mThread.start();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * When a message is received this method is called.
     * @param message A string holding the message received
     * @param session A session that represents the connection between this server and it client.
     */
    @OnMessage
    public String message(String message, Session session) {
        Gson gson = new GsonBuilder().create();
        SocketMessage update = gson.fromJson(message, SocketMessage.class);
        switch (update.getKey()) {
            case "request":
                System.out.println("Message from " + session.getId() + ": " + update.getValue());
                break;
            case "x":
                mWebSocket.sendMessage(message);
                System.out.println("Update X sent to server 1");
                break;
            case "y":
                mConnector.setSecsToSleep(Integer.parseInt(update.getValue()));
                System.out.println("ServerTwo updated Y with value: " + update.getValue());
                break;
            default:
                System.out.println("Error - wrong message format from client");
                System.out.println(message);
        }
        return message;
    }

    /**
     * When a connection is closed this method is called.
     * @param session A session that represents the connection between this server and it client.
     */
    @OnClose
    public void closeSession(Session session) {
        mThread.interrupt();
        System.out.println("Session " + session.getId() + " has ended.");
    }

    /**
     * Gets the URI of ServerOne from a .properties file
     * @return a String containing the URI of ServerOne
     */
    private String getServerURI() {
        String serverName = "";
        Properties properties = new Properties();
        String fileName = "server_config.properties";
        InputStream reader = getClass().getClassLoader().getResourceAsStream(fileName);
        if (reader != null) {
            try {
                properties.load(reader);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        serverName = properties.getProperty("SERVER_ONE");
        return serverName;
    }
}
