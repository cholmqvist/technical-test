package com.christian.comeon;

/**
 * Represents a message received by the socket holding information about
 * message type or other relevant information about the message (key) and the
 * actual message (value)
 * Created by Christian Holmqvist on 11/06/15.
 */
public class SocketMessage {
    private String mKey;
    private String mValue;

    /**
     * Constructor for SocketMessage
     * @param key the type of message this object contains
     * @param value the message this object contains
     */
    public SocketMessage(String key, String value) {
        mKey = key;
        mValue = value;
    }

    /**
     * Returns the key variable
     * @return the key variable of the object
     */
    public String getKey() {
        return mKey;
    }

    /**
     * Returns the value variable
     * @return the value variable of the object
     */
    public String getValue() {
        return mValue;
    }
}
