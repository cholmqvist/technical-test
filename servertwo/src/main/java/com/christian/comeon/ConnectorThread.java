package com.christian.comeon;

import com.google.gson.JsonObject;

import javax.websocket.Session;
import java.io.IOException;
import java.util.Random;

/**
 * A runnable class that accepts a Random Pair, an integer (y) and a websocket session, and
 * sends the variables the pair holds each Y seconds.
 * Created by christian on 09/06/15.
 */
public class ConnectorThread implements Runnable {
    private Session mSession;
    private RandomPair mPair;
    private int mSecsToSleep;

    public ConnectorThread(Session session, RandomPair pair, int y) {
        mSession = session;
        mPair = pair;
        mSecsToSleep = y;
    }

    /**
     * The run method starts a new thread that initiates a while loop that
     * goes on until the thread is interrupted. On each loop two variables are
     * extracted from a RandomPair and sent through a websocket Session,
     * after which the thread sleeps for a number of seconds.
     */
    public void run() {
        while (true) {
            try {
                JsonObject json = new JsonObject();
                json.addProperty("mKey", "random");
                json.addProperty("mValue", mPair.getLong()
                        + " "
                        + mPair.getString());
                mSession.getBasicRemote().sendText(json.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            long milliSecsToSleep = mSecsToSleep * 1000;
            try {
                Thread.sleep(milliSecsToSleep);
            } catch (InterruptedException ie) {
                return;
            }
        }
    }

    /**
     * Sets the number of seconds the thread will sleep
     * @param seconds number of seconds to sleep.
     */
    public void setSecsToSleep(int seconds) {
        mSecsToSleep = seconds;
    }
}
