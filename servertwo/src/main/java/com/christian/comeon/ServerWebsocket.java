package com.christian.comeon;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;

/**
 * Client endpoint of a websocket that handles communication with
 * another server.
 * Created by christian on 12/06/15.
 */
@ClientEndpoint
public class ServerWebsocket {
    private Session mSession;
    private MessageHandler mMessageHandler;

    /**
     * When initiated the websocket attempts to establish a connection
     * to a provided URI.
     * @param uri The URI that the socket should attempt to connect to.
     */
    public ServerWebsocket(URI uri) {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, uri);
        } catch (DeploymentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * When the connection is opened this method is called.
     * @param session The session representing the connection between this socket
     *                and the other server
     */
    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Opening websocket");
        mSession = session;
    }

    /**
     * When the connection is closed this method is called
     * @param session The session representing the connection between this socket
     *                and the other server
     * @param reason The reason for closing the socket
     */
    @OnClose
    public void onClose(Session session, CloseReason reason) {
        System.out.println("Closing websocket");
        mSession = null;
    }

    /**
     * When the socket receives a message this method is called. A custom message
     * handler needs to be added to the instance of the ServerWebsocket in order
     * to handle incoming messages.
     * @param message A String object holding the message received.
     */
    @OnMessage
    public void onMessage(String message) {
        if (mMessageHandler != null) {
            try {
                mMessageHandler.handleMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Adds a message handler to the instance of the ServerWebsocket
     * @param messageHandler A method that handles incoming messages
     */
    public void addMessageHandler(MessageHandler messageHandler) {
        mMessageHandler = messageHandler;
    }

    /**
     * Sends a message to the server endpoint of the websocket
     * @param message A String holding the message to be sent
     */
    public void sendMessage(String message) {
        try {
            mSession.getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * An inner interface defining one method that should be implemented
     * to handle incoming messages
     */
    public interface MessageHandler {
        void handleMessage(String message) throws IOException;
    }
}
