<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<style>
		body {
			background-color: #e6e6e6;
			font-family: sans-serif;
			width: 700px;
		}
		#input_div {
			padding: 10px;
			width: 540px;
			margin-left: auto;
			margin-right: auto;
		}
		#output_div {
			padding: 10px;
			background-color: #f2f2f2;
		}
		input[type="text"] {
			box-shadow: none;
  			border-radius: none;
  			padding: 6px;
  			font-size: 14px;
		}
		.btn {
			background-color:#44c767;
			-moz-border-radius:28px;
			-webkit-border-radius:28px;
			border-radius:28px;
			border:1px solid #18ab29;
			display:inline-block;
			cursor:pointer;
			color:#ffffff;
			font-family:Arial;
			font-size:14px;
			padding:6px 21px;
			text-decoration:none;
			text-shadow:0px 1px 0px #2f6627;
		}
	</style>
</head>
<body>
	<div id="input_div">
		<input class="inbox" type="text" id="inputX"><button class="btn" id="updateX">Set X</button>
		<input class="inbox" type="text" id="inputY"><button class="btn" id="updateY">Set Y</button>
	</div>
	</br>
	<div id="output_div">
		Random long: <div id="randLong"></div>
		</br>
		Random string: <div id="randString"></div>
	</div>
	<script type="text/javascript" src="randomsocket.js"></script>
</body>
</html>