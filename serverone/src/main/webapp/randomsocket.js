// This URI must be set as the address to an instance of
// ServerTwo
url = "ws://192.168.0.21:8080/serverTwo/randomServer";

var w = new WebSocket(url);
var randLong = document.getElementById("randLong");
var randString = document.getElementById("randString");

w.onopen = function() {
	var requestMessage = JSON.stringify({"mKey": "request", "mValue": "Socket request from update"});
    	w.send(requestMessage);
};
w.onerror = function(event) {
	console.log("Error");
};
w.onmessage = function(event) {
	var json = JSON.parse(event.data);
	if (json.mKey == "random") {
		var value = json.mValue;
		var received = value.split(" ");
		var long = received[0];
		var string = received[1];
		randLong.textContent = long;
		randString.textContent = string;
	} else {
		console.log(json.mValue);
	}


};
w.onclose = function(event) {
	console.log(event.data);
};

this.send = function(message) {
	this.waitForConnection(function() {
		w.send(message);
		console.log("Now ready state is set for random");
	}, 1000);
};

this.waitForConnection = function(callback, interval) {
	if (readyState === 1) {
		callback();
	} else {
		var that = this;
		setTimeout(function() {
			that.waitForConnection(callback, interval);
		}, interval);
	}
};
document.getElementById("updateX").onclick = function() {
	var mess = JSON.stringify({"mKey": "x", "mValue": document.getElementById("inputX").value});
	w.send(mess);
};
document.getElementById("updateY").onclick = function() {
	var mess = JSON.stringify({"mKey": "y", "mValue": document.getElementById("inputY").value});
	w.send(mess);
};