package com.christian.comeon;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

/**
 * A server endpoint class that accepts a connection from a client and
 * runs a RandomizerThread that handles this connection.
 * Created by christian on 09/06/15.
 */
@ServerEndpoint("/getRandoms")
public class ServerOne {
    private Thread mThread;
    private RandomizerThread mRandomizer;
    private Session mSession;

    /**
     * When a connection is opened this method is called. It starts a new thread
     * that handles the session object acquired.
     * @param session A session that represents the connection between this server and it client.
     */
    @OnOpen
    public void onOpen(Session session) {
        System.out.println(session.getId() + " has opened a connection");
        mSession = session;
        mRandomizer = new RandomizerThread(mSession, 2);
        mThread = new Thread(mRandomizer);
        mThread.start();
        try {
            JsonObject json = new JsonObject();
            json.addProperty("mKey", "request");
            json.addProperty("mValue", "Connection established for random");
            session.getBasicRemote().sendText(json.toString());
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * When a message is received this method is called.
     * @param message
     * @param session
     */
    @OnMessage
    public void message(String message, Session session) {
        Gson gson = new GsonBuilder().create();
        SocketMessage update = gson.fromJson(message, SocketMessage.class);
        switch (update.getKey()) {
            case "request":
                System.out.println("Message from " + session.getId() + ": " + update.getValue());
                try {
                    session.getBasicRemote().sendText(message);
                } catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                break;
            case "x":
                mRandomizer.setSecsToSleep(Integer.parseInt(update.getValue()));
                System.out.println("ServerOne updated X with value: " + update.getValue());
                try {
                    session.getBasicRemote().sendText(message);
                } catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                break;
            default:
                System.out.println("Error: wrong message format");
        }
    }

    /**
     * When a connection is closed this method is called.
     * @param session
     */
    @OnClose
    public void closeSession(Session session) {
        mThread.interrupt();
        System.out.println("Session " + session.getId() + " has ended.");
    }
}
