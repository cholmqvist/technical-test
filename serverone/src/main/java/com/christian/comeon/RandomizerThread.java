package com.christian.comeon;

import com.google.gson.JsonObject;

import javax.websocket.Session;
import java.io.IOException;
import java.util.Random;

/**
 * A runnable accepts a websocket session and an integer (x). It generates a random long and
 * random string and sends them to the remote client of the session whereby
 * it sleeps for X seconds.
 * Created by christian on 09/06/15.
 */
public class RandomizerThread implements Runnable {
    private Session mSession;
    private int mSecsToSleep;
    private static final String letters = "0123456789abcdefghijklmnopqrstuvxyz";
    public RandomizerThread(Session session, int x) {
        mSession = session;
        mSecsToSleep = x;
    }

    /**
     * The run method starts a new thread that initates a while loop that goes on until
     * the thread is interrupted. On each loop the method generates a random long and
     * a random string and sends it to the remote client of a provided websocket session.
     */
    public void run() {
        System.out.println("Thread going");
        long theLong;
        Random random = new Random();
        while (true) {
            theLong = random.nextLong();
            try {
                JsonObject json = new JsonObject();
                json.addProperty("mKey", "random");
                json.addProperty("mValue", String.valueOf(theLong) +
                        " " +
                        randomString(10));
                String jsonString = json.toString();
                mSession.getBasicRemote().sendText(jsonString);
            } catch (IOException e) {
                e.printStackTrace();
            }
            long milliSecsToSleep = mSecsToSleep * 1000;
            try {
                Thread.sleep(milliSecsToSleep);
            } catch (InterruptedException ie) {
                System.out.println("Thread interrupted");
                return;
            }
        }
    }

    /**
     * Generates and returns a random string object at a given length.
     * @param length The length of the generated random string
     * @return a random string.
     */
    private String randomString(int length) {
        Random random = new Random();
        StringBuilder builder = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            builder.append(letters.charAt(random.nextInt(letters.length())));
        }
        return builder.toString();
    }

    /**
     * Sets the number of seconds the thread will sleep.
     * @param seconds number of seconds to sleep.
     */
    public void setSecsToSleep(int seconds) {
        mSecsToSleep = seconds;
    }
}
